# Generated by Django 2.2.1 on 2019-05-31 13:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_customer_read_posts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='read_posts',
            field=models.CharField(blank=True, default='', max_length=1024, null=True),
        ),
    ]
