import json

from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models


class Customer(AbstractUser):
    name = models.CharField(max_length=100, blank=True, null=True)
    blog_obj = models.OneToOneField('core.Blog', on_delete=models.CASCADE, related_name='customer', null=True)

    read_posts = models.CharField(max_length=1024, blank=True, null=True, default='')  # TODO(bad practice) need to use postgres json field

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+777777777777'."
                                         " Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, verbose_name="phone number", null=True)

    def set_read_posts(self, x):
        self.read_posts = json.dumps(x)

    def get_read_posts(self):
        return json.loads(self.read_posts)
