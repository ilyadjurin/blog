from django.db import models

from apps.users.models import Customer


class Blog(models.Model):
    name = models.CharField(max_length=128)
    subscribe_users = models.ManyToManyField(Customer, related_name='blog', null=True, blank=True)

    class Meta:
        verbose_name = "blog"
        verbose_name_plural = "blogs"

    def __str__(self):
        return self.name


class Post(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, related_name='posts')
    title = models.CharField(max_length=200)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = "post"
        verbose_name_plural = "posts"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        ## TODO add mailing method to subscribe users
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        ## TODO add mailing method to subscribe users
        super(Post, self).delete(*args, **kwargs)
