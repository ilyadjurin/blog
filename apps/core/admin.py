from django.contrib import admin

from apps.core.models import Blog, Post


@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    filter_vertical = ('subscribe_users',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('blog', 'title', 'content')
