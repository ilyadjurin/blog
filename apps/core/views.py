import itertools

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views import View
from django.views.generic import TemplateView

from apps.core.models import Post, Blog


class BaseView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/'
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        list_exclude = []
        if request.user.read_posts != '':
            list_exclude = list(itertools.chain.from_iterable(request.user.get_read_posts().values()))
        context['posts'] = Post.objects.filter(blog__customer__in=request.user.blog_obj.subscribe_users.all()).exclude(
            id__in=list_exclude)
        context['blog'] = request.user.blog_obj
        context['my_posts'] = request.user.blog_obj.posts.all()
        return self.render_to_response(context)


class UnsubscribeView(LoginRequiredMixin, View):
    login_url = '/admin/'

    def post(self, request):
        blog = Blog.objects.filter(posts=request.POST.get('post_id'))
        request.user.blog_obj.subscribe_users.remove(blog.last().customer)
        if request.user.read_posts != '' and request.user.get_read_posts().get(str(blog.last().id)):
            read_dict = request.user.get_read_posts()
            read_dict.pop(str(blog.last().id), None)
            request.user.set_read_posts(read_dict)
            request.user.save()
        request.user.blog_obj.save()
        return redirect('/')


class MarkReadView(LoginRequiredMixin, View):
    login_url = '/admin/'

    def post(self, request):
        blog = Blog.objects.filter(posts=request.POST.get('post_id'))
        if request.user.read_posts != '':
            read_dict = request.user.get_read_posts()
            if read_dict.get(str(blog[0].id)):
                read_dict[str(blog[0].id)].append(request.POST.get('post_id'))
            else:
                read_dict[str(blog[0].id)] = [request.POST.get('post_id')]
            request.user.set_read_posts(read_dict)
        else:
            request.user.set_read_posts({blog[0].id: [request.POST.get('post_id')]})
        request.user.save()
        return redirect('/')


class NewPostView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/'
    template_name = 'new.html'


class CreatePostView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/'

    def post(self, request):
        title = 'title'
        if request.POST.get('title'):
            title = request.POST.get('title')
        content = 'content'
        if request.POST.get('content'):
            content = request.POST.get('content')
        Post.objects.create(title=title, content=content, blog=request.user.blog_obj)
        return redirect('/')
