
# Custom blog

Проект развернут на http://nekidaem.autoton.biz/


## Пользователи
Сейчас в системе (на сервере http://nekidaem.autoton.biz/) есть 3 пользователя.
- Админ(видит и может редактировать все) - admin/testpass , для пользователей anton и andre установлен
одинаковый пароль 1232testpass


## Развертывание проекта
```
cp .env_default .env && nano .env
docker-compose build
docker-compose up
```

```